# Favorite List

Favorite List é uma API que simula a resposta de uma listagem de produtos favoritados por clientes do Magazine Luiza.

## Setup do ambiente

### Requisitos

- docker
- docker-compose

### Iniciando a API

Para iniciar a API basta digitar `docker-compose up` na raiz do projeto. Para o correto funcionamento, é necessário rodar uma carga inicial na base de dados que irá inicializar dois pontos importantes:

- Criação de usuários: isso irá criar dois usuários com papéis e acessos diferentes à API e que serão usados para autenticar e autorizar as requisições;
- Criação e carga inicial de um cliente: isso irá criar um cliente e fará a carga inicial de 10 produtos na sua lista de favoritos, simulando um comportamento real de inserção de produtos em uma lista de favoritos.

Para fazer essa carga inicial, basta rodar o seguinte comando na raíz do projeto e com os containers rodando:

```
$ docker-compose run web rake db:seed
```

Essa carga inicial será feita consultando a API de produtos que foi fornecida no desafio técnico.

A partir daqui, a API já estará pronta para receber requisições em `http://localhost:3000`.

## Autenticação

Essa API utiliza Basic Authentication para autenticar as requisições, então é necessário incluir este cabeçalho com `username` e `password` em todas as requisições para os endpoints abaixo.

## Autorização

Apenas usuários autorizados podem acessar recursos específicos dessa API, para isso, a carga inicial cria dois `username` sendo eles:

- `client`: autorizado a acessar apenas a API de clientes;
- `favorite_list`: autorizado a acessar apenas a API de listagem de favoritos.

Ambos possuem a mesma senha em ambiente de desenvolvimento que é `test`.

## Endpoints

Para acessar os endpoints você deverá estar autenticado e ser autorizado.

### Clientes

#### `GET /clients/:id`

Retorna um JSON com detalhes de um cliente específico onde o parâmetro `:id` corresponde ao ID do cliente na base de dados. Para testar, é possível consultar o cliente com ID 1, que é criado na carga inicial.

A resposta dessa API terá esse formato:

```json
{
    "id": 1,
    "email": "test@test.com",
    "name": "test",
	"list_id": 1
}
```

#### `POST /clients/new`

Esse endpoint cria um novo cliente juntamente com sua lista de favoritos. O corpo da requisição deverá seguir o seguinte formato:

```json
{
    "email": "test2@test.com",
    "name": "test"
}
```

A resposta desse endpoint irá conter os detalhes do novo cliente:


```json
{
	"id": 2,
    "email": "test2@test.com",
    "name": "test"
}
```

#### `PUT /clients/:id`

Esse endpoint é utilizado para atualizar os atributos de um cliente já existente. O corpo da requisição deve conter as informações que você deseja atualizar e o parametro `:id` na URL será o ID do cliente a ser atualizado.

Exemplo de corpo quando o `email` do cliente deve ser atualizado:

```json
{
    "email": "novo@email.com"
}
```

#### `DELETE /clients/:id`

Esse endpoint deleta um cliente e sua lista de favoritos. O parâmetro `:id` é o ID do cliente a ser deletado. A resposta da API deverá ser a seguinte:

```json
{
    "message": "Client 1 deleted"
}
```

### Listagem de favoritos

#### `GET /favorite_products_list/:client_id?page=<PAGINA>`

Esse endpoint é responsável por exibir a listagem de produtos favoritos de um cliente. Os parâmetros da requisição são os seguintes:

- `:client_id`: ID do cliente que você deseja consultar a lista de favoritos;
- `page`: um inteiro que representa a página a ser exibida. Começa em 0 e se não for utilizado na requisição, exibe apenas a primeira página.
- `per_page`: indica o número de itens que devem ser exibidos por página. O valor padrão é 10 mas pode ser alterado.

O retorno dessa API é uma listagem com os produtos de um cliente e um metadado informando detalhes da paginação:

```json
{
    "meta": {
        "page": 0,
        "per_page": 10
    },
    "products": [
        {
            "price": 147.19,
            "image": "http://challenge-api.luizalabs.com/images/1bcd1b21-7205-4f02-227f-4c8c9e845ade.jpg",
            "brand": "nino",
            "id": "1bcd1b21-7205-4f02-227f-4c8c9e845ade",
            "title": "Farol Automotivo Lado Direito para Corsa 94 a 2000"
        }
    ]
}
```

#### `POST /favorite_products_list/add_product`

Esse endpoint pode ser utilizado para adicionar produtos a uma lista específica. O corpo da requisição deve seguir o seguinte formato:

```json

{
	"list_id": "ID_DA_LISTA",
	"product_id": "ID_DO_PRODUTO"
}
```

A resposta desse endpoint será:

```json
{
	"message": "Product <PRODUCT_ID> successfuly added to list <LIST_ID>"
}
```

Obs.: O `product_id` deve ser um ID válido disponível na API de produtos fornecida para este desafio.

## Comandos que podem ajudar

Caso você deseje reiniciar a base de dados para depois fazer uma nova carga inicial e recomeçar os testes do zero, basta digitar na raiz do projeto:

```
$ docker-compose run web rake app:reset
```