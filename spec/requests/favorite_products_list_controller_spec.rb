require 'rails_helper'

RSpec.describe FavoriteProductsListController, type: :controller do
  let(:client) { Client.create(email: 'test@test.com', name: 'test') }
  let(:meta) do
    {
      page: 0,
      per_page: 10
    }
  end
  let(:product_params) do
    {
      favorite_products_list_id: client.favorite_products_list.id,
      price: 100.0,
      image: '/image/5ac12dda-73cd-4a85-aee3-31612f0cb4f1',
      brand: 'Brand',
      title: 'Title',
      review_score: 5.0,
      product_id: '5ac12dda-73cd-4a85-aee3-31612f0cb4f1'
    }
  end
  let(:products) do 
    [
      {
        price: product_params[:price],
        image: product_params[:image],
        brand: product_params[:brand],
        id: product_params[:product_id],
        title: product_params[:title],
        review_score: product_params[:review_score] 
      } 
    ]
  end

  describe "GET /favorite_products_list/:client_id" do
    let(:expected_response) do
      {
        meta: meta,
        products: products
      }.to_json
    end

    context 'when there are no products to return' do
      let(:products) {[]}
      it 'lists products from the list' do
        get :list, params: { client_id: client.id }

        expect(response.body).to eq(expected_response)
      end
    end

    context 'when there are products to return' do
      before do 
        Product.create(product_params)
      end
      it 'lists products' do
        get :list, params: { client_id: client.id }

        expect(response.body).to eq(expected_response)
      end
    end
  end
  
  describe "POST /favorite_products_list/add_product" do
    context 'when params are ok' do
      before do
        allow(ProductsApi::Client).to receive(:details).and_return(product_params)
      end
      let(:expected_response) do
        {
          message: "Product #{product_params[:product_id]} successfuly added to list #{client.favorite_products_list.id}"
        }.to_json
      end

      it 'creates a product' do
        post :add_product, params: { list_id: client.favorite_products_list.id, product_id: product_params[:product_id] }
        
        expect(Product.count).to eq(1)
        expect(response.body).to eq(expected_response)
      end
    end

    context 'when there is another product with the same id' do
      before do
        allow(ProductsApi::Client).to receive(:details).and_return(products.first.with_indifferent_access)
        Product.create(product_params)
      end
      let(:expected_response) do
        {
          message: "Product #{product_params[:product_id]} already exists on the list #{client.favorite_products_list.id}"
        }.to_json
      end

      it 'does not creates the product' do
        post :add_product, params: { list_id: client.favorite_products_list.id, product_id: product_params[:product_id] }

        expect(Product.count).to eq(1)
        expect(response.body).to eq(expected_response)
      end
    end
  end
end

