require 'rails_helper'

RSpec.describe ClientsController, type: :controller do
  let(:client) { Client.create(email: 'test@test.com', name: 'name') }
  describe "GET /clients/:id" do
    let(:expected_response) do
      {
        id: client.id,
        email: client.email,
        name: client.name,
        list_id: client.favorite_products_list.id
      }.to_json
    end

    it 'returns client details' do
      get :show, params: { id: client.id }

      expect(response.body).to eq(expected_response)
    end
  end

  describe "PUT /clients/:id" do
    it 'updates the client' do
      put :update, params: { email: 'test2@test.com', id: client.id }

      expect(Client.find(client.id).email).to eq('test2@test.com')
    end
  end

  describe "DELETE /clients/:id" do
    it 'deletes the client' do
      delete :delete, params: { id: client.id }

      expect(Client.count).to eq(0)
    end
  end

  describe "POST /clients/new" do
    context 'when params are ok' do
      it 'creates a new client' do
        post :create, params: { name: 'test', email: 'test@test.com' }

        expect(Client.count).to eq(1)
      end
    end

    context 'when params are missing' do
      let(:expected_response) do
        {
          message: 'Validation failed: Email can\'t be blank'
        }.to_json
      end

      it 'does not create a new client' do
        post :create, params: { name: 'test' }

        expect(Client.count).to eq(0)
        expect(response.body).to eq(expected_response)
      end
    end

    context 'when the email is already in use' do
      before do
        Client.create(name: 'test', email: 'test@test.com')
      end
      let(:expected_response) do
        {
          message: "The email test@test.com is already in use"
        }.to_json
      end

      it 'raises an error' do
        post :create, params: { name: 'test', email: 'test@test.com' }

        expect(response.body).to eq(expected_response)
      end
    end
  end
end
