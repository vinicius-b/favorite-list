# Create new data
begin
  puts 'Seeding database...'
  User.create(username: 'favorite_list', password: 'test', role: :favorite_list)
  User.create(username: 'client', password: 'test', role: :client)
  c = Client.create(name: 'test', email: 'test@test.com')
  ProductGeneratorService.new(c.favorite_products_list.id).random_generate(10)
rescue ActiveRecord::RecordNotUnique
  puts "DB is already seeded"
end

