class CreateFavoriteProductsLists < ActiveRecord::Migration[6.1]
  def change
    create_table :favorite_products_lists do |t|
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
