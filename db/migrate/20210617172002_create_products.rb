class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.references :favorite_products_list
      t.float :price
      t.string :image
      t.string :brand
      t.string :title
      t.float :review_score
      t.string :product_id

      t.timestamps
    end

    add_index :products, [:product_id, :favorite_products_list_id], unique: true
  end
end
