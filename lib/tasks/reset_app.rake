namespace :app do
  desc "Reset application database"
  task :reset => :environment do
		kill_idle_connections_sql = "SELECT pg_terminate_backend(pg_stat_activity.pid) 
																 FROM pg_stat_activity 
																 WHERE datname='#{ActiveRecord::Base.connection.current_database}' AND state='idle';"

		ActiveRecord::Base.connection.execute(kill_idle_connections_sql)
    Rake::Task['db:drop'].invoke
    Rake::Task['db:create'].invoke
    Rake::Task['db:migrate'].invoke
  end
end
