class FavoriteProductsListController < ProtectedController
  PAGINATION_LIMIT = 10

  def list
    @page = params.fetch(:page, 0).to_i
    @per_page = params.fetch(:per_page, PAGINATION_LIMIT).to_i
    client_id = params[:client_id]
    list = FavoriteProductsList.find_by!(client_id: client_id)
    products = list.products.offset(@page * @per_page).limit(@per_page)
    response = format_response(products)

    render json: response
  rescue ActiveRecord::RecordNotFound
    render json: { message: "Favorite list not found for client #{client_id}" }, status: :not_found
  end

  def add_product
    @list_id = params[:list_id]
    @product_id = params[:product_id]
    ProductGeneratorService.new(@list_id).generate_from_product_id(@product_id)

    render json: { message: "Product #{@product_id} successfuly added to list #{@list_id}"}, status: :ok
  rescue ActiveRecord::RecordNotUnique
    render json: { message: "Product #{@product_id} already exists on the list #{@list_id}" }
  rescue ProductGeneratorService::ProductNotfoundError
    render json: { message: "Product #{@product_id} not found" }, status: :not_found
  end

  private

  def format_response(products)
    formated_products = products.map do |product|
      result = {
        price: product.price,
        image: product.image,
        brand: product.brand,
        id: product.product_id,
        title: product.title
      }
      result.merge!(review_score: product.review_score) if product.review_score.present?
      
      result
    end

    {
      meta: {
        page: @page,
        per_page: @per_page
      },
      products: formated_products
    }
  end
end
