class ClientsController < ProtectedController
  def create
    name = params[:name]
    email = params[:email]
    client = Client.create!(email: email, name: name)

    render json: { id: client.id, email: client.email, name: client.name }, status: :created
  rescue ActiveRecord::RecordNotUnique
    render json: { message: "The email #{params[:email]} is already in use" }
  rescue ActiveRecord::RecordInvalid => e
    render json: { message: e.message }
  end

  def update
    id = params[:id]
    client = Client.find(id)
    client.update!(params.permit(:name, :email))

    render json: { id: client.id, email: client.email, name: client.name }
  rescue ActiveRecord::RecordNotFound
    render json: { message: 'Client not found' }
  rescue ActiveRecord::RecordInvalid => e
    render json: { message: e.message }
  end

  def show
    client = Client.find(params[:id])

    render json: { id: client.id, email: client.email, name: client.name, list_id: client.favorite_products_list.id }
  rescue ActiveRecord::RecordNotFound
    render json: { message: 'Client not found' }
  end

  def delete
    id = params[:id]
    client = Client.find(id)
    client.destroy

    render json: { message: "Client #{client.id} deleted" }
  rescue ActiveRecord::RecordNotFound
    render json: { message: 'Client not found' }
  end
end
