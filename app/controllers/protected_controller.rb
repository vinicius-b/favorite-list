class ProtectedController < ApplicationController
  before_action :authenticate, :authorize, unless: proc { Rails.env.test? }

  def authenticate
    username, password = ActionController::HttpAuthentication::Basic::user_name_and_password(request)
    @user = User.find_by(username: username)

    render json: { message: 'Unauthenticated request' }, status: :unauthorized unless @user&.password == password
  end

  def authorize
    result = false

    result = case @user&.role
             when 'favorite_list'
               params[:controller] == 'favorite_products_list'
             when 'client'
               params[:controller] == 'clients'
             else
               false
             end

    render json: { message: 'Unauthorized request' }, status: :forbidden unless result
  end
end
