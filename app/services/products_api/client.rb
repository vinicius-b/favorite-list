module ProductsApi
  class Client
    PRODUCTS_API_URL = 'https://challenge-api.luizalabs.com/api/product/'

    class << self
      def list(page)
        uri = URI(PRODUCTS_API_URL)
        uri.query = URI.encode_www_form(page: page)

        response = make_request(uri)

        response['products']
      end

      def details(product_id)
        uri = URI("#{PRODUCTS_API_URL}#{product_id}/")

        make_request(uri)
      end

      private

      def make_request(uri)
        response = Net::HTTP.get(uri)

        JSON.parse(response)
      end
    end
  end
end
