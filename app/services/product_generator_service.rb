class ProductGeneratorService
  class ProductNotfoundError < StandardError; end
  PRODUCTS_API_URL = 'https://challenge-api.luizalabs.com/api/product/'

  def initialize(favorite_products_list_id)
    @list_id = favorite_products_list_id
  end

  def random_generate(n)
    page = rand(1..100)
    products = ProductsApi::Client.list(page)

    if products.present?
      sample = products.sample(n)

      sample.each do |params|
        params['product_id'] = params.delete('id')
        params['review_score'] = params.delete('reviewScore')
        params.merge!(favorite_products_list_id: @list_id)

        Product.create(params)
      end
    end
  end

  def generate_from_product_id(product_id)
    product = ProductsApi::Client.details(product_id)

    raise ProductNotfoundError if product['code'] == 'not_found'

    product['product_id'] = product.delete('id')
    product['review_score'] = product.delete('reviewScore')
    product.merge!(favorite_products_list_id: @list_id)

    Product.create(product)
  end
end
