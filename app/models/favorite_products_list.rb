class FavoriteProductsList < ApplicationRecord
  belongs_to :client
  has_many :products
end
