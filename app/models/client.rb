class Client < ApplicationRecord
  has_one :favorite_products_list, dependent: :destroy
  after_create :create_favorite_products_list

  validates :name, presence: true
  validates :email, presence: true
end
