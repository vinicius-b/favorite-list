  Rails.application.routes.draw do
    ## Clients endpoints

    get '/clients/:id', to: 'clients#show'
    post '/clients/new', to: 'clients#create'
    put '/clients/:id', to: 'clients#update'
    delete '/clients/:id', to: 'clients#delete'

    ## Favorite list endpoints
    get '/favorite_products_list/:client_id', to: 'favorite_products_list#list'
    post '/favorite_products_list/add_product', to: 'favorite_products_list#add_product'
end
